#!/bin/bash

SERVER_IP="51.89.14.126"
WORKING_DIR="/home/aiden/application"

set -e

BOT_USERNAME=$1
cd .sessions
zip -r session.zip "chrome_$BOT_USERNAME"
scp -r session.zip aiden@$SERVER_IP:$WORKING_DIR/.sessions/
rm session.zip

ssh -t aiden@$SERVER_IP "cd $WORKING_DIR/.sessions/ && unzip session.zip && rm session.zip && cd $WORKING_DIR && python3 install_profile.py $BOT_USERNAME"
