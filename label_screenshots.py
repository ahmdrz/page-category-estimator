from collections import defaultdict
from glob import glob
from os import mkdir
from os.path import join, basename, exists, expanduser
from shutil import copy

influencers_data = join('data', 'influencers.txt')
screenshots_path = join(expanduser("~"), 'screenshots')
dataset_directory = join(expanduser("~"), 'influencers_dataset')


def main():
    usernames = {}
    with open(influencers_data, 'r') as handler:
        lines = handler.readlines()

    labels = defaultdict(int)
    for line in lines:
        parts = line.split('\t')
        username = parts[0].strip()
        category = parts[1].strip()
        usernames[username] = category

    for screenshot in glob(join(screenshots_path, '*.jpg')):
        username = basename(screenshot)[:-4]
        try:
            category = usernames[username]
        except KeyError:
            category = 'other'

        category_path = join(dataset_directory, category)
        if not exists(category_path):
            mkdir(category_path)

        labels[category] += 1
        copy(screenshot, category_path)

    for key, value in labels.items():
        print("{}: {:,}".format(key, value))

if __name__ == "__main__":
    main()
