from PIL import Image


def read_image(image_path):
    return Image.open(image_path)


def resize_image(image, width=None, height=None):
    assert not (width is None and height is None)

    w, h = image.size
    if width and not height:
        ratio = width / w
        height = round(h * ratio)
    elif height and not width:
        ratio = height / h
        width = round(w * ratio)

    return image.resize((width, height))
