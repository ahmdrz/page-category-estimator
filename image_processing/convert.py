from io import BytesIO


def save_image_as_jpeg(input_image, **jpeg_params):
    default_params = dict(
        format='JPEG',
        quality=75,
        optimize=True,
    )
    for default_key, default_value in default_params.items():
        if default_key not in jpeg_params:
            jpeg_params[default_key] = default_value

    bytes_object = BytesIO()
    input_image.convert('RGB').save(bytes_object, **jpeg_params)
    bytes_object.seek(0)
    return bytes_object
