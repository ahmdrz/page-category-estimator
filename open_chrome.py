import sys
import time

from screenshot.chrome_manager import PersistentChrome, get_user_data_dir


def main():
    username = sys.argv[1]
    user_data = get_user_data_dir(username)
    chrome = PersistentChrome(user_data, headless=False)
    chrome.get('https://instagram.com/')
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        pass
    chrome.close()


if __name__ == "__main__":
    main()
