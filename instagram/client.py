from .categories import get_category_index
from .session import login, get_session_storage_path


def bool_to_number(value):
    if value:
        return 1
    return 0


class InstagramClient:
    def __init__(self, username, password):
        self._client = login(username, password, get_session_storage_path(username))

    def user_information(self, target_username):
        data = self._client.username_info(target_username)
        status = data.get('status', 'nok')
        if status != 'ok':
            return
        user = data.get('user')
        if not user:
            return

        return {
            'pk': user.get('pk'),
            'account_type': user.get('account_type'),
            'category': get_category_index(user.get('category')),
            'is_verified': bool_to_number(user.get('is_verified')),
            'is_private': bool_to_number(user.get('is_private')),
            'media_count': user.get('media_count')
        }
