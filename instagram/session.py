import codecs
import json
import os

try:
    from instagram_private_api import (
        Client, ClientError, ClientLoginError,
        ClientCookieExpiredError, ClientLoginRequiredError,
        __version__ as client_version)
except ImportError:
    import sys

    sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
    from instagram_private_api import (
        Client, ClientError, ClientLoginError,
        ClientCookieExpiredError, ClientLoginRequiredError,
        __version__ as client_version)


def get_session_storage_path(username):
    if not os.path.exists('.sessions'):
        os.mkdir('.sessions')

    return os.path.join('.sessions', username + '.session')


def to_json(python_object):
    if isinstance(python_object, bytes):
        return {'__class__': 'bytes',
                '__value__': codecs.encode(python_object, 'base64').decode()}
    raise TypeError(repr(python_object) + ' is not JSON serializable')


def from_json(json_object):
    if '__class__' in json_object and json_object['__class__'] == 'bytes':
        return codecs.decode(json_object['__value__'].encode(), 'base64')
    return json_object


def onlogin_callback(api, new_settings_file):
    cache_settings = api.settings
    with open(new_settings_file, 'w') as outfile:
        json.dump(cache_settings, outfile, default=to_json)


class InstagramLoginError(Exception):
    pass


def login(username, password, session_storage):
    device_id = None
    try:
        if not os.path.isfile(session_storage):
            api = Client(username, password,
                         on_login=lambda x: onlogin_callback(x, session_storage))
        else:
            with open(session_storage) as file_data:
                cached_settings = json.load(file_data, object_hook=from_json)
            device_id = cached_settings.get('device_id')
            api = Client(username, password, settings=cached_settings)

    except (ClientCookieExpiredError, ClientLoginRequiredError) as e:
        api = Client(username, password,
                     device_id=device_id,
                     on_login=lambda x: onlogin_callback(x, session_storage))

    except ClientLoginError as e:
        raise InstagramLoginError('ClientLoginError {0!s}'.format(e))
    except ClientError as e:
        raise InstagramLoginError(
            'ClientError {0!s} (Code: {1:d}, Response: {2!s})'.format(e.msg, e.code, e.error_response))
    except Exception as e:
        raise InstagramLoginError('Unexpected Exception: {0!s}'.format(e))
    return api
