import os
import sys
from distutils.util import strtobool

from decouple import config

from botnet import get_random_username, mark_as_unhealthy, is_anonymous
from image_processing.convert import save_image_as_jpeg
from image_processing.io import read_image, resize_image
from screenshot.chrome_manager import PersistentChrome, get_user_data_dir, DEVICE_TYPE_IOS
from screenshot.exceptions import ScreenshotError, UnhealthyProfile, UnknownException, save_html_source


def take_screenshot(username, screenshot_path, bot_username=None):
    if bot_username:
        from screenshot.screenshot import get_profile_screenshot
    else:
        from screenshot.anonymous import get_profile_screenshot

    chrome = PersistentChrome(
        user_data_dir=get_user_data_dir(bot_username),
        headless=config('CHROME_HEADLESS', True, cast=strtobool),
        device_type=config('DEVICE_TYPE', DEVICE_TYPE_IOS),
    )
    chrome.implicitly_wait(0)
    try:
        get_profile_screenshot(chrome, username, 'screenshot.png')
        image = read_image('screenshot.png')
        image = resize_image(image, width=640)
        jpeg_bytes = save_image_as_jpeg(image)
        with open(screenshot_path, 'wb') as handler:
            handler.write(jpeg_bytes.getbuffer())
        os.remove('screenshot.png')
    except UnknownException as e:
        save_html_source(chrome, username, bot_username)
        raise UnknownException(e)
    except UnhealthyProfile as e:
        save_html_source(chrome, username, bot_username)
        raise UnhealthyProfile(e)
    finally:
        chrome.quit()


def main():
    username = sys.argv[1]
    bot_username = get_random_username() if not is_anonymous() else None
    if bot_username:
        print("~ {} picked as chrome profile.".format(bot_username))

    try:
        screenshot_path = 'screenshot.jpg'
        take_screenshot(username, screenshot_path, bot_username)
        print("~ {} screenshot saved.".format(username))
    except ScreenshotError as e:
        print("~ {} got an error. {}.".format(username, e))
    except UnhealthyProfile:
        print("~ {} is unhealthy profile.".format(bot_username))
        mark_as_unhealthy(bot_username)


if __name__ == "__main__":
    main()
