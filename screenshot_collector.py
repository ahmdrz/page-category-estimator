import os
import time

from decouple import config

from botnet import touch, mark_as_unhealthy, get_next_profile_in_queue
from screenshot.exceptions import ScreenshotError, UnhealthyProfile, UnknownException
from take_screenshot import take_screenshot


def pick_username(text_file):
    file = open(os.path.join('data', text_file), 'r')

    def find_user():
        while True:
            line = file.readline()
            if not line:
                break

            username = line[:-1].split('\t')[0].strip()
            screenshot_path = os.path.join('screenshots', username + '.jpg')
            invalid_user_path = os.path.join(os.path.join('.sessions', 'invalid_user'), username)
            if os.path.exists(screenshot_path) or os.path.exists(invalid_user_path):
                continue
            return username

    username = find_user()
    file.close()
    return username


if __name__ == "__main__":
    max_trials = config('SCREENSHOT_TRIALS', 10, cast=int)

    if not os.path.exists('screenshots'):
        os.mkdir('screenshots')

    invalid_user_dir = os.path.join('.sessions', 'invalid_user')
    if not os.path.exists(invalid_user_dir):
        os.mkdir(invalid_user_dir)

    screenshot_trials = 0
    while screenshot_trials < max_trials:
        try:
            bot_username = get_next_profile_in_queue()
            print("~ {} picked as chrome profile.".format(bot_username))
        except ValueError as e:
            print("! profiles: {}".format(e))
            break

        username = pick_username(config('USERS_LIST'))
        screenshot_path = os.path.join('screenshots', username + '.jpg')
        invalid_user_path = os.path.join(invalid_user_dir, username)
        try:
            print("- trying to reach {} profile ...".format(username))
            take_screenshot(username, screenshot_path, bot_username)
            print("~ {} screenshot saved.".format(username))
        except ScreenshotError as e:
            print("~ {} got an error. {}.".format(username, e))
            touch(invalid_user_path)
        except UnhealthyProfile as e:
            print("~ {} marked as unhealthy profile.".format(bot_username))
            mark_as_unhealthy(bot_username)
        except UnknownException as e:
            print("~ chrome needs attention.")
            break
        except Exception as e:
            print("~ {} unknown exception. {}.".format(username, e))
        finally:
            screenshot_trials += 1
