import os

from decouple import config
from selenium.webdriver import Chrome, ChromeOptions, TouchActions

DEVICE_TYPE_IOS = 'iOS'
DEVICE_TYPE_ANDROID = 'android'


def get_user_data_dir(username):
    if not os.path.isdir('.sessions'):
        os.mkdir('.sessions')

    if not username:
        return os.path.join('.sessions', 'chrome_anonymous')

    return os.path.join('.sessions', 'chrome_{}'.format(username))


class PersistentChrome(Chrome):
    device_types = {
        DEVICE_TYPE_IOS: 'iPhone X',
        DEVICE_TYPE_ANDROID: 'Pixel 2',
    }

    def __init__(self, user_data_dir=None, headless=True, device_type=DEVICE_TYPE_IOS):
        chrome_options = ChromeOptions()
        chrome_options.add_argument("disable-infobars")

        if headless:
            chrome_options.add_argument("--headless")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--window-size=1920,1200")
        chrome_options.add_argument("--ignore-certificate-errors")
        chrome_options.add_argument("--disable-extensions")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_experimental_option("mobileEmulation", {
            "deviceName": self.device_types[device_type],
        })
        chrome_options.add_experimental_option('w3c', False)

        if user_data_dir:
            os.makedirs(user_data_dir, exist_ok=True)
            chrome_options.add_argument('--user-data-dir={}'.format(user_data_dir))
        super().__init__(executable_path=config('CHROME_DRIVER', 'chromedriver'), chrome_options=chrome_options)

    def tap_on(self, selector):
        elements = self.find_element_by_css_selector(selector)
        touchactions = TouchActions(self)
        touchactions.tap(elements).perform()
