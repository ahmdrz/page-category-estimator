import time

from screenshot.chrome_manager import PersistentChrome
from screenshot.elements import (
    wait_for_scroll,
    close_dialogs,
    accept_cookies,
    is_unhealthy,
    is_unavailable,
    is_private,
    click_on_search_button,
    type_in_search_input,
    click_on_back_to_search_button,
    close_notification_dialog,
    get_article_element, dismiss_activity_notification,
)
from screenshot.exceptions import UnhealthyProfile, ScreenshotError


def get_profile_screenshot(chrome_instance: PersistentChrome, username: str, output_path: str):
    print("- loading instagram ...")
    chrome_instance.get('https://www.instagram.com/')
    chrome_instance.set_page_load_timeout(15)
    chrome_instance.set_script_timeout(15)

    print("- checking profile health ...")
    if is_unhealthy(chrome_instance):
        raise UnhealthyProfile('This profile is unhealthy')

    print("- dismissing notification dialog ...")
    close_notification_dialog(chrome_instance)

    print("- checking accept cookies dialog ...")
    accept_cookies(chrome_instance)
    close_dialogs(chrome_instance)

    print("- clicking on search button ...")
    click_on_search_button(chrome_instance)

    print("- type username in search input ...")
    result = type_in_search_input(chrome_instance, username)
    if not result:
        chrome_instance.get('https://www.instagram.com/{}/'.format(username))

    time.sleep(1)

    print("- checking profile health ...")
    if is_unhealthy(chrome_instance):
        raise UnhealthyProfile('This profile is unhealthy')

    print("- checking user page ...")
    if is_unavailable(chrome_instance):
        raise ScreenshotError('This page is not available')

    print("- finding article selector ...")
    grid_bar_element = get_article_element(chrome_instance)
    if not grid_bar_element:
        raise UnhealthyProfile('Article not found')

    print("- checking private status ...")
    if is_private(grid_bar_element):
        raise ScreenshotError('This page is private')

    print("- scrolling ...")
    has_enough_items = wait_for_scroll(chrome_instance, grid_bar_element)
    if not has_enough_items:
        raise ScreenshotError('This page has not enough posts')

    print("- taking screenshot ...")
    time.sleep(1)
    dismiss_activity_notification(chrome_instance)
    chrome_instance.save_screenshot(output_path)

    print("- back to search page ...")
    click_on_back_to_search_button(chrome_instance)
    time.sleep(1)
