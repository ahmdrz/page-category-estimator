import time

from screenshot.chrome_manager import PersistentChrome
from screenshot.elements import (
    hide_app_installation,
    close_dialogs,
    wait_for_scroll,
    show_more_posts,
    accept_cookies,
)
from screenshot.exceptions import ScreenshotError, UnknownException


def get_profile_screenshot(chrome_instance: PersistentChrome, username: str, output_path: str):
    profile_link = 'https://www.instagram.com/{}/'.format(username)
    chrome_instance.get(profile_link)
    chrome_instance.set_page_load_timeout(15)
    chrome_instance.set_script_timeout(15)

    hide_app_installation(chrome_instance)
    close_dialogs(chrome_instance)
    accept_cookies(chrome_instance)

    grid_bar_elements = chrome_instance.find_elements_by_css_selector('article')
    if not grid_bar_elements:
        raise UnknownException('Article not found')
    grid_bar_element = grid_bar_elements[0]

    show_more_posts(grid_bar_element, username)

    has_enough_items = wait_for_scroll(chrome_instance, grid_bar_element)
    if not has_enough_items:
        raise ScreenshotError('This page has not enough posts')

    time.sleep(1)
    chrome_instance.save_screenshot(output_path)
