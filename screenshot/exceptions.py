import os
import time

from screenshot.chrome_manager import PersistentChrome


class ScreenshotError(Exception):
    pass


class UnhealthyProfile(Exception):
    pass


class UnknownException(Exception):
    pass


def save_html_source(chrome_instance: PersistentChrome, username, profile_username):
    if not os.path.isdir('.sessions'):
        os.mkdir('.sessions')

    if not os.path.isdir(os.path.join('.sessions', 'html_source')):
        os.mkdir(os.path.join('.sessions', 'html_source'))

    html_source = chrome_instance.page_source
    time.sleep(1)
    with open(os.path.join('.sessions', 'html_source', '{}_{}.html'.format(username, profile_username)), 'w') as handler:
        handler.write(html_source)
