import time
from collections import Iterable

from selenium.common.exceptions import ElementClickInterceptedException
from selenium.webdriver.common.keys import Keys

from screenshot.chrome_manager import PersistentChrome


class Timeout(Iterable):
    def __iter__(self):
        return self

    def __init__(self, seconds):
        self.seconds = seconds * 10
        self.current = 0

    def __next__(self):
        if self.current == self.seconds:
            raise StopIteration
        self.current += 1
        time.sleep(0.1)


def close_notification_dialog(chrome_instance):
    accept_buttons = chrome_instance.find_elements_by_xpath('//button[text()="Not Now"]')
    if not accept_buttons:
        return
    try:
        accept_buttons[0].click()
    except ElementClickInterceptedException:
        pass


def hide_app_installation(chrome_instance):
    for _ in Timeout(2):
        elements = chrome_instance.find_elements_by_xpath('''//section//div[text()="Not Now"]''')
        if elements:
            element = elements[0]
            element.click()
            break


def wait_for_scroll(chrome_instance, grid_bar_element):
    for _ in Timeout(10):
        chrome_instance.execute_script("arguments[0].scrollIntoView();", grid_bar_element)
        items = grid_bar_element.find_elements_by_css_selector('a[href^="/p"]')
        if len(items) > 20:
            return True
    return False


def get_article_element(chrome_instance):
    for _ in Timeout(3):
        grid_bar_elements = chrome_instance.find_elements_by_css_selector('article')
        if grid_bar_elements:
            return grid_bar_elements[0]


def hide_username(chrome_instance):
    navigation_bar = chrome_instance.find_element_by_css_selector('nav')
    username_box = navigation_bar.find_element_by_css_selector('h1')
    chrome_instance.execute_script("arguments[0].innerText = 'username'", username_box)


def is_unavailable(chrome_instance):
    elements = chrome_instance.find_elements_by_xpath('''//h2[text()="Sorry, this page isn't available."]''')
    return len(elements) > 0


def is_private(grid_bar_element):
    elements = grid_bar_element.find_elements_by_xpath('''//h2[text()="This Account is Private"]''')
    return len(elements) > 0


def dismiss_activity_notification(chrome_instance):
    elements = chrome_instance.find_elements_by_css_selector("a[href='/accounts/activity/']")
    if not elements:
        return

    element = elements[0]
    buttons = element.find_elements_by_css_selector("div[role='button']")
    if not buttons:
        return
    chrome_instance.execute_script("""
    var element = arguments[0];
    element.parentNode.removeChild(element);
    """, buttons[0])


def is_unhealthy(chrome_instance: PersistentChrome):
    if 'Add Phone Number'.lower() in chrome_instance.title.lower():
        return True
    elements = chrome_instance.find_elements_by_xpath('''//h2[text()="Confirm it's You to Login"]''')
    return len(elements) > 0


def click_on_search_button(chrome_instance):
    for _ in Timeout(3):
        search_elements = chrome_instance.find_elements_by_css_selector("a[href='/explore/']")
        if search_elements:
            search_elements[0].click()
            break


def click_on_back_to_search_button(chrome_instance):
    search_element = chrome_instance.find_element_by_css_selector("a[href='/explore/search/']")
    search_element.click()


def type_in_search_input(chrome_instance, username):
    for _ in Timeout(2):
        search_elements = chrome_instance.find_elements_by_css_selector("input[type='search']")
        if search_elements:
            search_element = search_elements[0]
            search_element.click()
            search_element.send_keys(username)
            search_element.send_keys(Keys.ENTER)
            break

    for _ in Timeout(5):
        user_elements = chrome_instance.find_elements_by_css_selector("ul li a[href^='/']")
        if len(user_elements) != 0:
            break

    for _ in Timeout(2):
        user_elements = chrome_instance.find_elements_by_css_selector("a[href='/{}/']".format(username))
        if user_elements:
            user_elements[0].click()
            return True
    return False


def close_dialogs(chrome_instance):
    classes_to_close = ['glyphsSpriteGrey_Close', 'coreSpriteDismissLarge']
    for class_to_close in classes_to_close:
        for button in chrome_instance.find_elements_by_class_name(class_to_close):
            try:
                button.click()
            except ElementClickInterceptedException:
                pass


def accept_cookies(chrome_instance):
    accept_buttons = chrome_instance.find_elements_by_xpath('//button[text()="Accept"]')
    if not accept_buttons:
        return
    try:
        accept_buttons[0].click()
    except ElementClickInterceptedException:
        pass


def show_more_posts(grid_bar_element, username):
    for _ in Timeout(3):
        elements = grid_bar_element.find_elements_by_xpath('''//button//div[text()="Show More Posts from {}"]'''.format(
            username
        ))
        if elements:
            element = elements[0]
            element.click()
            break
