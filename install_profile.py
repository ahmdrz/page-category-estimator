import sys

from botnet import get_total_usernames

if __name__ == "__main__":
    profile = sys.argv[1]
    profiles = get_total_usernames()
    if profile in profiles:
        exit(0)

    with open('.profiles', 'a') as handler:
        handler.write('{}\n'.format(profile))
