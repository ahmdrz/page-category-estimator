import os
import pathlib
import random

import requests
from decouple import config


def get_total_usernames():
    # return config('INSTAGRAM_USERNAME', cast=Csv())
    with open('.profiles', 'r') as handler:
        lines = handler.readlines()
    return [line.strip() for line in lines if not line.startswith("#") and len(line) > 3]


def touch(file_path):
    from pathlib import Path

    Path(file_path).touch()


def send_message_telegram(text_message):
    requests.post(
        url='https://api.telegram.org/bot{0}/sendMessage'.format(config('TELEGRAM_BOT_TOKEN')),
        data={'chat_id': config('TELEGRAM_BOT_CHATID'), 'text': text_message}
    )


def get_unhealthy_profile_path(username):
    unhealthy_profiles = os.path.join('.sessions', 'unhealthy_profiles')
    if not os.path.isdir(unhealthy_profiles):
        os.mkdir(unhealthy_profiles)
    return os.path.join(unhealthy_profiles, username)


def mark_as_unhealthy(username):
    username_path = get_unhealthy_profile_path(username)
    touch(username_path)
    send_message_telegram('{} is unhealthy profile.'.format(username))


def mark_as_healthy(username):
    username_path = get_unhealthy_profile_path(username)
    if os.path.exists(username_path):
        os.remove(username_path)


def is_unhealthy(username):
    username_path = get_unhealthy_profile_path(username)
    return os.path.exists(username_path)


def get_healthy_profiles():
    healthy_profiles = []
    for username in get_total_usernames():
        if is_unhealthy(username):
            continue
        healthy_profiles.append(username)
    return healthy_profiles


def get_profile_username(index):
    usernames = get_total_usernames()
    return usernames[index]


def get_profile_password(index):
    return config('INSTAGRAM_PASSWORD')


def get_random_username():
    total_usernames = get_healthy_profiles()
    length = len(total_usernames)
    if length == 0:
        raise ValueError("not enough profiles.")
    index = random.randint(0, length - 1)
    return total_usernames[index]


def last_used(username):
    if not os.path.isdir(os.path.join('.sessions', 'profiles')):
        os.mkdir(os.path.join('.sessions', 'profiles'))
    file_path = os.path.join('.sessions', 'profiles', username)
    if not os.path.exists(file_path):
        return 0
    file = pathlib.Path(file_path)
    return int(file.stat().st_mtime)


def mark_as_selected(username):
    if not os.path.isdir(os.path.join('.sessions', 'profiles')):
        os.mkdir(os.path.join('.sessions', 'profiles'))
    file_path = os.path.join('.sessions', 'profiles', username)
    touch(file_path)


def get_next_profile_in_queue():
    total_usernames = get_healthy_profiles()
    length = len(total_usernames)
    if length == 0:
        raise ValueError("not enough profiles.")
    modification_times = ["{},{}".format(last_used(username), username) for username in total_usernames]
    modification_times.sort()
    target = modification_times[0]
    username = target.split(',')[1]
    mark_as_selected(username)
    return username


def is_anonymous():
    anonymous = config('BOTNET_ANONYMOUS', False, cast=bool)
    profiles = get_healthy_profiles()
    return anonymous or len(profiles) == 0
