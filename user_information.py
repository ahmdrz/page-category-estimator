import json
import sys

from decouple import config

from instagram import InstagramClient


def save_object(o, file_name):
    with open(file_name, 'w') as handler:
        json.dump(o, handler)


def main():
    target_username = sys.argv[1]
    username = config('INSTAGRAM_USERNAME')
    password = config('INSTAGRAM_PASSWORD')
    instance = InstagramClient(username, password)
    data = instance.user_information(target_username)
    save_object(data, 'development/user.json')


if __name__ == "__main__":
    main()
